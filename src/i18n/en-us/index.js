export default {
  common: {
    appName: 'Telos Communities',
    buttons: {
      cancel: 'Cancel',
      continue: 'Continue',
      create: 'Create',
      logout: 'Logout',
      mint: 'Mint',
      register: 'Register',
      save: 'Save',
      confirm: 'Confirm',
      accept: 'Accept',
      reject: 'Reject'
    },
    defaultTitle: 'Title'
  },
  forms: {
    errors: {
      accountFormat: 'The account can contain lowercase characters only, numbers from 1 to 5 or a dot (.)',
      accountFormatBasic: 'The account must contain 12 lowercase characters only and number from 1 to 5',
      accountLength: 'The account must contain at most 12 characters',
      accountNotAvailable: 'The account "{account}" already exists',
      accountNotExists: 'The account "{account}" does not exist',
      copyKey: 'Copy the key to a safe place',
      dateFuture: 'The date must be in the future',
      greaterOrEqualThan: 'The value must be greater than than or equal to {value}',
      integer: 'Please type an integer',
      phoneFormat: 'Please type a valid phone',
      positiveInteger: 'The value must be greater than 0',
      required: 'This field is required',
      token: 'The field must contain between 2 and 6 characters',
      tokenDecimals: 'The decimals must be between 2 and 9'
    },
    hints: {
      selectCountrie: 'Please select your country',
      pressToAddHobbie: 'Press enter to add a tag',
      pressToAddURL: 'Press enter to add a URL'
    }
  },
  lists: {
    empty: {
      countries: 'No countries'
    }
  },
  pages: {
    login: {
      title: 'Select your wallet',
      getApp: 'Download the app',
      selectAccount: 'Please select an account',
      welcome: 'Hi',
      q1: 'Authorize',
      q2: 'to use your account?',
      thisAppCan: 'This application will be able to:',
      change_account: 'Change account',
      create_account: 'Do you want to create an account?',
      not_have_account: 'You do not have a Telos account.'
    },
    verifyProfile: {
      verifySMS: 'Verify SMS',
      codeSMS: 'Verification code',
      verifyEMAIL: 'Verify EMAIL',
      codeEMAIL: 'Verification code'
    },
    accounts: {
      add: {
        forms: {
          phoneCode: 'Code'
        }
      }
    },
    index: {
      buttons: {
        createAccount: 'Create account',
        login: 'Login with Telos'
      }
    },
    signUp: {
      form: {
        presentation: 'Bio',
        name: 'Name',
        firstName: 'First Name',
        lastName: 'Last Name',
        preferMethodComm: 'Preferred method of communication',
        sms: 'SMS Number',
        email: 'Email',
        currentSms: 'Current SMS Number',
        currentEmail: 'Current Email',
        timeZone: 'Time Zone',
        tags: 'Tags',
        btnSave: 'Save',
        newCustomFieldName: 'Write the name of new custom field',
        editCustomFieldName: 'Write the new field name',
        addCustomField: 'Add custom field'
      }
    },
    general: {
      noAccountsFound: 'No accounts found',
      confirmActions: 'Do you want confirm this action?'
    }
  }
}
