import PPP from '@smontero/ppp-client-api'
// import SSOServices from '~/services/SSOServices'

const getAuthenticator = function (ual, wallet = null) {
  wallet = wallet || localStorage.getItem('autoLogin')
  const idx = ual.authenticators.findIndex(auth => auth.constructor.name === wallet)
  return {
    authenticator: ual.authenticators[idx],
    idx
  }
}

export const loginSSO = async function ({ commit, dispatch }, { idx, account, returnUrl }) {
  const authenticator = this.$ual.authenticators[idx]
  console.log(authenticator)
  try {
    commit('setLoadingWallet', authenticator.getStyle().text)
    commit('setAutoLogin', true)
    await authenticator.init()
    if (!account) {
      const requestAccount = await authenticator.shouldRequestAccountName()
      if (requestAccount) {
        await dispatch('fetchAvailableAccounts', idx)
        commit('setRequestAccount', true)
        return null
      }
    }
    const users = await authenticator.login(account)
    console.log('Login SSO', users)
    if (users.length) {
      this.$ualUser = users[0]
      this.$type = 'ual'
      const accountName = await users[0].getAccountName()
      commit('setAccount', accountName)
      // const defaultReturnUrl = localStorage.getItem('returning') ? '/profiles/chat' : '/profiles/myProfile'
      PPP.setActiveUser(this.$ualUser)
      const authApi = PPP.authApi()
      const validSession = await authApi.hasValidSession()
      console.log('authApi.hasValidSession()', validSession)
      if (validSession === false) {
        await authApi.signIn()
      }
      localStorage.setItem('autoLogin', authenticator.constructor.name)
      await this.dispatch('profiles/getProfile', { root: true })
      localStorage.setItem('account', accountName)
      // localStorage.setItem('returning', true)
      // this.$router.push({ path: returnUrl || defaultReturnUrl })
      return this.$ualUser
    }
  } catch (e) {
    const error = (authenticator.getError() && authenticator.getError().message) || e.message || e.reason
    commit('general/setErrorMsg', error, { root: true })
    console.log('Login error: ', error)
    return null
  } finally {
    commit('setLoadingWallet')
    commit('setAutoLogin', false)
  }
}

export const logoutSSO = async function ({ commit }) {
  await PPP.authApi().signOut()
  commit('setAutoLogin', true)
  const { authenticator } = getAuthenticator(this.$ual)
  console.log(authenticator)
  try {
    authenticator && await authenticator.logout()
  } catch (error) {
    console.log('Authenticator logout error', error)
  }
  commit('profiles/setProfile', null, { root: true })
  commit('setAccount', null)
  localStorage.removeItem('autoLogin')
  this.$api = null
  commit('setAutoLogin', false)
  return 'Logout success'
  // this.$router.push({ path: '/' })
}

export const autoLoginSSO = async function ({ dispatch, commit }) {
  const { authenticator, idx } = getAuthenticator(this.$ual)
  console.log('autoLoginSSO', authenticator)
  let user = null
  if (authenticator) {
    commit('setAutoLogin', true)
    // commit('setAuthenticatorUal', authenticator)
    // await dispatch('login', { idx, returnUrl, account: localStorage.getItem('account') })
    user = await dispatch('loginSSO', { idx, account: localStorage.getItem('account') })
    commit('setAutoLogin', false)
    /* if (!user) {
      await dispatch('logoutSSO', { idx, account: localStorage.getItem('account') })
    } */
    console.log('Se logueo con auto Login', user)
  }
  return user
}

export const isSessionSavedSSO = async function ({ dispatch, state, store }) {
  const { authenticator } = getAuthenticator(this.$ual)
  if (authenticator) console.log('isSessionSavedSSO', authenticator.users)
  console.log('isSessionSavedSSO store', store)
  console.log('isSessionSavedSSO State', state)
  if (authenticator) {
    return true
  }
  return false
}

export const hasValidSession = async function ({ dispatch, state, store }) {
  try {
    const validSession = await PPP.authApi().hasValidSession()
    return validSession
  } catch (e) {
    console.error(e)
    return false
  }
}

export const userInfo = async function ({ dispatch, state, store }) {
  try {
    const userInfo = await PPP.authApi().userInfo()
    return userInfo
  } catch (e) {
    console.error(e)
    return false
  }
}

export const authContextSSO = async function ({ commit }, { params }) {
  try {
    // const SSOApi = new SSOServices()
    // const auth = await SSOApi.authContext(params)
    commit('setAutoLogin', true)
    const oauthApi = PPP.oauthApi()
    const auth = await oauthApi.authContext(params)
    console.log('PPP Auth Context', auth)
    return {
      success: true,
      response: auth
    }
  } catch (e) {
    console.log('Error catch', e.message)
    return {
      success: false,
      error: e
    }
  } finally {
    commit('setAutoLogin', false)
  }
}

export const authCodeSSO = async function ({ commit }, { params }) {
  try {
    // const SSOApi = new SSOServices()
    // const auth = await SSOApi.authCode(params)
    commit('setAutoLogin', true)
    const oauthApi = PPP.oauthApi()
    const authCode = await oauthApi.authCode(params)
    console.log('PPP Auth Code', authCode)
    return {
      success: true,
      response: authCode
    }
  } catch (e) {
    console.log('Error catch', e)
    return {
      success: false,
      error: e
    }
  } finally {
    commit('setAutoLogin', false)
  }
}

export const sendOTP = async function ({ commit }, form) {
  try {
    const response = await this.$axios.post('/v1/registrations', {
      smsNumber: form.internationalPhone,
      telosAccount: form.account
    })
    if (response) {
      commit('setSignUpForm', form)
    }
    return true
  } catch (e) {
    return {
      error: e.message
    }
  }
}

export const verifyOTP = async function ({ commit, state }, { password, publicKey }) {
  try {
    await this.$axios.post('/v1/accounts', {
      smsOtp: password,
      smsNumber: state.signUpForm.internationalPhone,
      telosAccount: state.signUpForm.account,
      ownerKey: publicKey,
      activeKey: publicKey
    })
    return {
      success: true
    }
  } catch (e) {
    return {
      success: false,
      error: e.message
    }
  }
}

export const fetchAvailableAccounts = async function ({ commit }, idx) {
  commit('resetAvailableAccounts')
  const chainId = process.env.NETWORK_CHAIN_ID
  const authenticator = this.$ual.authenticators[idx]
  const map = await authenticator.getAccountNamesPerChain()
  const accounts = map.has(chainId) ? map.get(chainId) : []
  commit('setAvailableAccounts', accounts)
}
