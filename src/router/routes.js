const routes = [
  { path: '/', component: () => import('pages/login/loginSSO.vue'), meta: { layout: 'guest', title: 'pages.login.title' } },
  { path: '*', component: () => import('pages/404.vue'), meta: { layout: 'empty' } }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/404.vue'),
    meta: { layout: 'empty' }
  })
}

export default routes
